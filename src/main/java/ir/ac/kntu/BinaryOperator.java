package ir.ac.kntu;

@FunctionalInterface
public interface BinaryOperator<T> {

    public T apply(T firstOperand, T secondOperand);
}
