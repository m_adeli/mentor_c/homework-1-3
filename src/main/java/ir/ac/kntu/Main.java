package ir.ac.kntu;

/**
 * @author Mohsen Adeli
 */
public class Main {
    public static void main(String[] args) {
        // method 1
        BinaryOperator<Integer> add1 = new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer firstOperand, Integer secondOperand) {
                return firstOperand + secondOperand;
            }
        };

        BinaryOperator<Integer> subtract1 = new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer firstOperand, Integer secondOperand) {
                return firstOperand - secondOperand;
            }
        };

        BinaryOperator<Integer> multiply1 = new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer firstOperand, Integer secondOperand) {
                return firstOperand * secondOperand;
            }
        };

        BinaryOperator<Integer> divide1 = new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer firstOperand, Integer secondOperand) {
                return firstOperand / secondOperand;
            }
        };

        System.out.println(add1.apply(5, 9));
        System.out.println(subtract1.apply(17, 6));
        System.out.println(multiply1.apply(4, 9));
        System.out.println(divide1.apply(81, 9));
        System.out.println("-----------------------------------");

        // method 2 (Lambda)
        BinaryOperator<Integer> add2 = (x, y) -> Math.addExact(x, y);
        BinaryOperator<Integer> subtract2 = (x, y) -> Math.subtractExact(x, y);
        BinaryOperator<Integer> multiply2 = (x, y) -> Math.multiplyExact(x, y);
        BinaryOperator<Integer> divide2 = (x, y) -> Math.floorDiv(x, y);

        System.out.println(add2.apply(5, 9));
        System.out.println(subtract2.apply(17, 6));
        System.out.println(multiply2.apply(4, 9));
        System.out.println(divide2.apply(81, 9));

        System.out.println("-----------------------------------");

        // method 3 (method reference)
        BinaryOperator<Integer> add3 = Math::addExact;
        BinaryOperator<Integer> subtract3 = Math::subtractExact;
        BinaryOperator<Integer> multiply3 = Math::multiplyExact;
        BinaryOperator<Integer> divide3 = Math::floorDiv;

        System.out.println(add3.apply(5, 9));
        System.out.println(subtract3.apply(17, 6));
        System.out.println(multiply3.apply(4, 9));
        System.out.println(divide3.apply(81, 9));

        //These are Known as Function Pointers in C/C++ and Delegates in C#
    }
}
